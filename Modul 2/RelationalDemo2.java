public class RelationalDemo2 {
	public static void main (String[] args) {
		//berapa angka
		int i=37;
		int j=42;
		int k=42;
		
		System.out.println("Nilai variable...");
		System.out.println("i="+i);
		System.out.println("j="+j);
		System.out.println("k="+k);

		//Lebih besar dari
		System.out.println("Lebih besar...");
		System.out.println("i>j="+(i>j)); //false
		System.out.println("j>i="+(j>i)); //true
		System.out.println("k>j="+(k>j)); //false

		//Lebih besar atau sama dengan
		System.out.println("Lebih besar atu sama dengan...");
		System.out.println("i>=j="+(i>=j)); //false
		System.out.println("j>=i="+(j>=i)); //true
		System.out.println("k>=j="+(k>=j)); //true

		//Lebih kecil
		System.out.println("Lebih kecil...");
		System.out.println("i<j="+(i<j)); 
		System.out.println("j<i="+(j<i)); 
		System.out.println("k<j="+(k<j)); 

		//Lebih kecil atau sama dengan
		System.out.println("Lebih kecil atau sama dengan...");
		System.out.println("i<=j="+(i<=j)); 
		System.out.println("j<=i="+(j<=i)); 
		System.out.println("k<=j="+(k<=j)); 

		//Sama dengan
		System.out.println("Sama dengan...");
		System.out.println("i=j="+(i==j)); 
		System.out.println("k=j="+(k==j)); 

		//Tidak sama dengan
		System.out.println("Lebih besar atu sama dengan...");
		System.out.println("i!=j="+(i!=j)); 
		System.out.println("k!=j="+(k!=j)); 
	}
}