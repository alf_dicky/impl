public class AritmetikaDemo {
	public static void main (String[] args) {
		//some numbers
		int i=37;
		int j=42;
		double x=27.475;
		double y=7.22;
		
		System.out.println("\nVariable values...");
		System.out.println("i="+i);
		System.out.println("j="+j);
		System.out.println("x="+x);
		System.out.println("y="+y);
		
		//Adding numbers
		System.out.println("\nAdding...");
		System.out.println("i+j="+(i+j)); //harus bernilai 79
		System.out.println("x+y="+(x+y));

		//Substraction
		System.out.println("\nSubstracing...");
		System.out.println("i-j="+(i-j));
		System.out.println("x-y="+(x-y));

		//Number Multiplier
		System.out.println("\nMultiplying...");
		System.out.println("i*j="+i*j);
		System.out.println("x*y="+x*y);

		//Devide number
		System.out.println("\nDividing...");
		System.out.println("i/j="+(i/j));
		System.out.println("x/y="+(x/y));

		//Counting the modulus result
		System.out.println("\nComputing the reminder...");
		System.out.println("i%j="+i%j);
		System.out.println("x%y="+(x%y));

		//Mixing type
		System.out.println("\nMixing tipes...");
		System.out.println("j+y="+(j+y)); //analisa
		System.out.println("i*x="+(i*x)); //analisa
	}
}




